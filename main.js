'use strict';

class Employee{
    constructor(name, age, salary){
        this.name = name
        this.age = age
        this.salary = salary
    }

    get name(){
        return `Name is ${this._name}`;
    }

    set name(value){
        if(value.length == 0){
            console.log("Please, enter correct name");
            return;
        }
        this._name = value;
    }

    get age(){
        return `${this._name} is ${this._age}  years old `;
    }

    set age(value){
        if(value > 18 && value < 65){
            this._age = value;
            return;
        } else {
            console.error("Incorrect age");
        }
    }

    get salary(){
        return `${this._name}'s  salary is ${this._salary} $`;
    }

    set salary(value){
        this._salary = value;
    }
}


class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this.lang = lang
    }

    get salary(){
        return `${this._name}'s  salary is ${this._salary} $`;
    }

    set salary(value){
        this._salary = value*3;
    }

}

let em1 = new Programmer("Oksana", 42, 1500, ["english", "polish", "ukrainian"]);
let em2 = new Programmer("Kristina", 35, 1200, ["english", "polish", "ukrainian", "german"]);
let em3 = new Programmer("Mykhailo", 28, 1050, ["english", "polish", "ukrainian"]);

console.log(em1);
console.log(em2);
console.log(em3);
